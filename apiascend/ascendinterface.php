<?php

	
use Magento\Framework\App\Bootstrap;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require '../app/bootstrap.php';
require('./config.php');

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objManager = $bootstrap->getObjectManager();
$state = $objManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$apikeys = $config['apikeys'];
$apikey = $_GET["apikey"];

header('Content-type: text/xml');
header('Pragma: public');
header('Cache-control: private');
header('Expires: -1');
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

if( ! isset($apikey) || !array_key_exists( $apikey, $apikeys)) {
	echo "<Error>Invalid Credentials</Error>";
	return;
}


$now = new \DateTime();
$store_id = $apikeys[ $apikey ]['store_id'];

$orderCollection = $objManager
                 ->create('Magento\Sales\Model\ResourceModel\Order\Collection')
                 ->addFieldToSelect('*')
                 ->addFieldToFilter('export_status', ['eq' => 0])
                 ->addFieldToFilter('store_id', ['eq' => $store_id]);

echo "<WebOrders>
        <NumberOfOrders>".$orderCollection->count()."</NumberOfOrders>";

$objManager->create('Magento\Framework\Model\ResourceModel\Iterator')
           ->walk($orderCollection->getSelect(), array('orderCallback'));

function orderCallback($args)
{	
    $objManager = \Magento\Framework\App\ObjectManager::getInstance();	
    
    $order = $objManager->create('Magento\Sales\Model\Order')->load($args['row']['entity_id']);	
    $billing = $order->getBillingAddress();
    $shipping = $order->getShippingAddress();
    $time = date( "D M d H:i:s Y", strtotime($order->getData('created_at')));
	
    echo "<WebOrder>";    
        echo "<Order>";    
            echo "<OrderId>".$order->getData('entity_id')."</OrderId>";
            echo "<Currency>".$order->getData('order_currency_code')."</Currency>";
            echo "<Time>".$time."</Time>";
            echo "<CustomerId>".$order->getData('customer_id')."</CustomerId>";        
            
            echo "<AddressShipTo>";
                echo "<Name>";
                    echo "<First>".$shipping->getData('firstname')."</First>";
                    echo "<Last>".$shipping->getData('lastname')."</Last>";
                    echo "<Full>".$shipping->getData('firstname') . " " .$shipping->getData('lastname')."</Full>";
                echo "</Name>";
                echo "<Address1>".$shipping->getData('street')."</Address1>";
                echo "<Address2/>";
                echo "<City>".$shipping->getData('city')."</City>";
                echo "<State/>";
                echo "<Country>".$shipping->getData('country_id')."</Country>";
                echo "<Zip>".$shipping->getData('postcode')."</Zip>";
                echo "<Phone>".$shipping->getData('telephone')."</Phone>";
                echo "<Email>".$shipping->getData('email')."</Email>";
            echo "</AddressShipTo>";       

            echo "<AddressBillTo>";
                echo "<Name>";
                    echo "<First>".$billing->getData('firstname')."</First>";
                    echo "<Last>".$billing->getData('lastname')."</Last>";
                    echo "<Full>".$billing->getData('firstname') . " " .$billing->getData('lastname')."</Full>";
                echo "</Name>";
                echo "<Address1>".$billing->getData('street')."</Address1>";
                echo "<Address2/>";
                echo "<City>".$shipping->getData('city')."</City>";
                echo "<State/>";
                echo "<Country>".$shipping->getData('country_id')."</Country>";
                echo "<Zip>".$shipping->getData('postcode')."</Zip>";
                echo "<Phone>".$shipping->getData('telephone')."</Phone>";
                echo "<Email>".$shipping->getData('email')."</Email>";
            echo "</AddressBillTo>";

            echo "<Shipping>";
                echo "<Method>".$order->getData('shipping_method')."</Method>";
                echo "<Classification>". htmlspecialchars( $order->getData('shipping_description')) ."</Classification>";
            echo "</Shipping>";
            echo "<PaymentInfo>";
                echo "<PaymentMethod>".$order->getPayment()->getData('method')."</PaymentMethod>";
                echo "<CreditCardInfo>";
                    echo "<CreditCard>";
                    echo "<Type/>";
                    echo "</CreditCard>";
                echo "</CreditCardInfo>";
                //echo "<additional_information>".json_encode($order->getPayment()->getData('additional_information'))."</additional_information>";
            echo "</PaymentInfo>";
            echo "<Comments/>";
             
            echo "<Items>";
            
            $items = $order->getAllVisibleItems();
            foreach ( $items as $num => $item) {
                
                echo "<Item num=\"" . $num . "\">";

                    $product = $objManager->create('Magento\Catalog\Model\Product')->load($item->getData('product_id'));            
                    $catname = array();
                    $categories = $product->getCategoryIds();
                    
                    foreach($categories as $category){
                        $cat = $objManager->create('Magento\Catalog\Model\Category')->load($category);
                        $catname[] = $cat->getName();
                    }
                    
                    echo "<Id>".$item->getData('item_id')."</Id>";
                    echo "<Code/>";
                    echo "<Mpn/>";
                    echo "<Gtin1/>";
                    echo "<Gtin2>".$item->getData('sku')."</Gtin2>";
                    echo "<Quantity>".floor( $item->getData('qty_ordered'))."</Quantity>";
                    echo "<Unit-Price>".$item->getData('base_price')."</Unit-Price>";
                  
                    echo "<Weight>".$item->getData('weight')."</Weight>";
                    echo "<Description>".$item->getData('description')."</Description>";
                    echo "<Category>".implode(" > ",$catname)."</Category>";
                    echo "<Url>".$product->getProductUrl()."</Url>";                
                    echo "<Taxable>True</Taxable>";
                    echo "<ModelYear/>";
                echo "</Item>";
            }
            echo "</Items>";
            echo "<OrderTotal>";
                echo "<Line type=\"Discount\">". $order->getData("base_discount_amount") ."</Line>";
                echo "<Line type=\"Shipping\">". $order->getData("base_shipping_amount") ."</Line>";
                echo "<Line type=\"Subtotal\">". $order->getData("base_subtotal") ."</Line>";
                echo "<Line type=\"Tax\">". $order->getData("base_tax_amount") ."</Line>";
                echo "<Line type=\"Total\">". $order->getData("base_grand_total") ."</Line>";
            echo "</OrderTotal>";
        echo "</Order>"; 
        echo "<WebComment></WebComment>";
    echo "</WebOrder>";
}
echo "</WebOrders>";