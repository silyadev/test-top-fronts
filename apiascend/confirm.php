<?php

	
use Magento\Framework\App\Bootstrap;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require '../app/bootstrap.php';
require('./config.php');

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objManager = $bootstrap->getObjectManager();
$state = $objManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$apikeys = $config['apikeys'];
$apikey = $_GET["apikey"];
$orderId = $_GET["order"];

header('Content-type: text/xml');
header('Pragma: public');
header('Cache-control: private');
header('Expires: -1');
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

if( ! isset($apikey) || !array_key_exists( $apikey, $apikeys)) {
	echo "<Error>Invalid Credentials</Error>";
	return;
}

if( ! isset($orderId) ) {
	echo "<Error>Order ID required, but not provided</Error>";
	return;
}

$orderModel = $objManager->create('\Magento\Sales\Model\Order');

$order = $orderModel->load($orderId);
if( $order->getExportStatus() == 1 ) {
	echo "<Order>Status already updated</Order>";
} elseif( $order->getId()) {
	$order->setExportStatus(1);
	$order->save();
	echo "<Order>Status updated for ID ". $orderId."</Order>";
} else {
	echo "<Order>Invalid OrderId</Order>";
}