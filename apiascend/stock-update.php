<?php
	use Magento\Framework\App\Bootstrap;

	require '../app/bootstrap.php';
	require('./config.php');

	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objManager = $bootstrap->getObjectManager();
	$state = $objManager->get('Magento\Framework\App\State');
	$state->setAreaCode('frontend');

	$stockRegistry = $objManager->create('\Magento\CatalogInventory\Api\StockRegistryInterface');

	$zip = new ZipArchive;
	$lineFeed = "\n";
	$delimiter = "\t";
	$invArray[] = null;

	if ( $zip->open('../inv-upload/catsync.zip') === TRUE ) {
		
    	// Read inventory content
		$content = $zip->getFromName('inv.txt');
    	
		// close zip file.
		$zip->close();

    	$inventory = explode($lineFeed, $content);

    	foreach ( $inventory as $item ) {
			$properties = explode( $delimiter, $item );
			$sku = $properties[0];
			$qty = $properties[11];
			
			$stockRegistry->getStockItemBySku( $properties[0] )->setQty( $qty )
				
        	array_push( $invArray, $properties );
    	}
		echo "<pre>";
		print_r($array);
		echo "</pre>";

	} else {
    	echo 'not such file /inv-upload/catsync.zip';
	}