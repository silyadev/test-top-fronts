<?php

namespace Fahrad\Checkout\Block\Adminhtml;

/**
 * Class CustomerComment
 * @package Fahrad\Checkout\Block\Adminhtml
 */
class CustomerComment extends \Magento\Backend\Block\Template
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Sales\Model\Order $order,
        array $data = []
    ) {
        $this->order = $order;
        parent::__construct($context, $data);
    }

    /**
     * @return false|\Magento\Sales\Model\Order
     */
    private function getCurrentOrder()
    {
        $order = $this->order->load($this->getRequest()->getParam('order_id'));
        if (!$order->getId()) {
            return false;
        }

        return $order;
    }

    /**
     * @return false|string
     */
    public function getCustomerComment()
    {
        $order = $this->getCurrentOrder();

        if (!$order || !$order->getCustomerComment()) {
            return false;
        }

        return $order->getCustomerComment();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->_scopeConfig->getValue(
            'checkout/customer_comment/title'
        );
    }
}
