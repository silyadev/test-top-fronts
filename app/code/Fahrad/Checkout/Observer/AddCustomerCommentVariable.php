<?php

namespace Fahrad\Checkout\Observer;

/**
 * Class AddCustomerCommentVariable
 * @package Fahrad\Checkout\Observer
 */
class AddCustomerCommentVariable implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $transport = $observer->getTransport();
        $transport['customer_comment'] = $transport->getOrder()->getCustomerComment();
    }
}
