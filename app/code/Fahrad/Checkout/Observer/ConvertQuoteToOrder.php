<?php

namespace Fahrad\Checkout\Observer;

/**
 * Class ConvertQuoteToOrder
 * @package Fahrad\Checkout\Observer
 */
class ConvertQuoteToOrder implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * ConvertQuoteToOrder constructor.
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     */
    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory
    ) {
        $this->quoteFactory = $quoteFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getOrder();
        $quoteId = $order->getQuoteId();
        $quote  = $this->quoteFactory->create()->load($quoteId);
        $order->setCustomerComment($quote->getCustomerComment());
        $order->save();
    }
}
