<?php

namespace Fahrad\Checkout\Plugin\Checkout;

/**
 * Class LayoutProcessor
 * @package Fahrad\Checkout\Plugin\Checkout
 */
class LayoutProcessor
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     * LayoutProcessor constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        if (!$this->isEnabled()) {
            return $jsLayout;
        }

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['before-shipping-method-form']['children']['customer_comment'] = [
            'component' => 'Fahrad_Checkout/js/form/element/textarea',
            'config' => [
                'customScope' => 'shippingAddress',
                'template' => 'ui/form/field',
                'options' => [],
                'id' => 'customer_comment'
            ],
            'dataScope' => 'shippingAddress.customer_comment',
            'provider' => 'checkoutProvider',
            'visible' => true,
            'sortOrder' => 0,
            'id' => 'comment',
            'placeholder' => $this->getPlaceholder(),
            'field_title' => $this->getTitle()
        ];

        return $jsLayout;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function isEnabled()
    {
        return $this->scopeConfig->getValue(
            'checkout/customer_comment/customer_comment_enabled',
            'store',
            $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getTitle()
    {
        return $this->scopeConfig->getValue(
            'checkout/customer_comment/title',
            'store',
            $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getPlaceholder()
    {
        return $this->scopeConfig->getValue(
            'checkout/customer_comment/placeholder',
            'store',
            $this->storeManager->getStore()->getId()
        );
    }
}
