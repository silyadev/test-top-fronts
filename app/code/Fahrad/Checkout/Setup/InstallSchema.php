<?php

namespace Fahrad\Checkout\Setup;

/**
 * Class InstallSchema
 * @package Fahrad\Checkout\Setup
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'customer_comment',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'visible' => true,
                'comment' => 'Custom Comment'
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'customer_comment',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'visible' => true,
                'comment' => 'Customer Comment'
            ]
        );
    }
}
