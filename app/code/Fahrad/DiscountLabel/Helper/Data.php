<?php
namespace Fahrad\DiscountLabel\Helper;

/**
 * Class Data
 *
 * @package Fahrad\DiscountLabel\Helper
 */
class Data extends \MGS\Mpanel\Helper\Data
{
    public function getProductLabel($product){
        $html = '';
        $newLabel = $this->getStoreConfig('mpanel/catalog/new_label');
        $saleLabel = $this->getStoreConfig('mpanel/catalog/sale_label');
        $soldLabel = __('Out of Stock');
        // Out of stock label
        if (!$product->isSaleable()){
            $html .= '<span class="product-label sold-out-label"><span>'.$soldLabel.'</span></span>';
        }else {
            // New label
            $numberLabel = 0;
            $now = $this->getCurrentDateTime();
            $dateTimeFormat = \Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT;
            $newFromDate = $product->getNewsFromDate();
            if($newFromDate) {
                $newFromDate = date($dateTimeFormat, strtotime($newFromDate));
            }
            $newToDate = $product->getNewsToDate();
            if($newToDate) {
                $newToDate = date($dateTimeFormat, strtotime($newToDate));
            }
            if($newLabel != ''){
                if(!(empty($newToDate))){
                    if(!(empty($newFromDate)) && ($newFromDate < $now) && ($newToDate > $now)){
                        $html.='<span class="product-label new-label"><span>'.$newLabel.'</span></span>';
                        $numberLabel = 1;
                    }
                }
            }

            // Sale label
            $price = $product->getPrice();
            $finalPrice = $product->getFinalPrice();
            if ($product->getTypeId() == 'configurable') {
                $childProducts = $product->getTypeInstance()->getUsedProducts($product);
                if (count($childProducts)) {
                    $price = $childProducts[0]->getPrice();
                }
            }
            if($this->getStoreConfig('mpanel/catalog/sale_label_discount') == 1){
                if(($finalPrice<$price)){
                    $save = $price - $finalPrice;
                    $percent = round(($save * 100) / $price);
                    if($numberLabel == 1){
                        $html .= '<span class="product-label sale-label multiple-label"><span>-'.$percent.'%</span></span>';
                    }else{
                        $html .= '<span class="product-label sale-label"><span>-'.$percent.'%</span></span>';
                    }
                }
            }else {
                if(($finalPrice<$price) && ($saleLabel!='')){
                    if($numberLabel == 1){
                        $html .= '<span class="product-label sale-label multiple-label"><span>'.$saleLabel.'</span></span>';
                    }else{
                        $html .= '<span class="product-label sale-label"><span>'.$saleLabel.'</span></span>';
                    }
                }
            }
        }
        return $html;
    }
}
